import React from "react";
import PropTypes from "prop-types";

export default function ModalCard({ icon, headline, smallText }) {
  return (
    <div className="modal">
      <div className="modal__image">
        <img src={icon} alt="hands clapping" />
      </div>
      <h1>{headline}</h1>
      <p>{smallText}</p>
    </div>
  );
}
ModalCard.propTypes = {
  headline: PropTypes.string.isRequired,
  smallText: PropTypes.string.isRequired,
  icon: PropTypes.string,
};
