import { render } from '@testing-library/react';
import ModalCard from '../components/ModalCard';

test('renders modal card', () => {
    const props = {
        headline: 'Good Job!',
        smallText: 'You are done'
    } ;
    render(<ModalCard {...props}/>)
})