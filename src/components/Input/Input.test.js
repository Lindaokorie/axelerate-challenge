import { render } from '@testing-library/react';
import Input from '../components/Input';

test('renders input', () => {
    const props = {
        inputType: 'text',
        name: 'Name',
        handleInputChange: jest.fn(),
        value: ''
    } ;
    render(<Input {...props}/>)
})

