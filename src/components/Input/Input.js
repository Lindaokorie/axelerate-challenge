import React from "react";
import PropTypes from "prop-types";

import "./Input.scss";

export default function Input({
  className,
  inputType,
  name,
  value,
  handleInputChange,
  checked,
}) {
  const classNameField = `input ${className}`;
  return (
    <input
      type={inputType}
      name={name}
      value={value}
      className={classNameField}
      onChange={handleInputChange}
      checked={checked}
      required
    />
  );
}

Input.propTypes = {
  inputType: PropTypes.oneOf(["text", "tel", "email", "radio"]),
  handleInputChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.any,
  className: PropTypes.string,
  checked: PropTypes.bool,
};
