import { fireEvent, render } from '@testing-library/react';
import Modal from '../components/Modal'

test('renders modal and closes on click', () => {
    const handleClose= jest.fn();

    const {getByTestId} = render(<Modal handleClick={handleClose}/>)

    expect(getByTestId('modal')).toBeInTheDocument();

    fireEvent.click(getByTestId('modal'));

    expect(handleClose).toHaveBeenCalledTimes(1);
})