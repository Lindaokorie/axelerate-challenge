import React, { useEffect } from "react";
import { createPortal } from "react-dom";
import PropTypes from "prop-types";

import ModalCard from "../ModalCard/ModalCard";
import hands from "../../images/hands.svg";
import "./Modal.scss";

const modalElement = document.createElement("div");
modalElement.setAttribute("id", "modal-root");
document.body.appendChild(modalElement);

const headline = "Great Job!";
const tagline = "Your configurations have been updated successfully.";

export default function Modal({ handleClick }) {
  const el = document.createElement("div");

  useEffect(() => {
    modalElement.appendChild(el);
    return () => {
      modalElement.removeChild(el);
    };
  });

  return createPortal(
    <div data-testid="modal" className="overlay" onClick={handleClick}>
      <ModalCard icon={hands} headline={headline} smallText={tagline} />
    </div>,
    el
  );
}
Modal.propTypes = {
  handleClick: PropTypes.func.isRequired,
};
