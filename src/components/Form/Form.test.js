import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import Form from '../components/Form';

test('renders form and opens modal on submit', async () => {
    const {getByTestId} = render(<Form />);
    fireEvent.submit(getByTestId('form'));
    await waitFor(() => screen.getByTestId('modal'));
    const overlay = getByTestId('modal');
    expect(overlay).toBeInTheDocument();
})
