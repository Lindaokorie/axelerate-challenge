import React, { useState } from "react";

import Input from "../Input/Input";
import ConfigCard from "../ConfigCard/ConfigCard";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import exit from "../../images/exit.svg";
import switchIcon from "../../images/switch.svg";
import hero from "../../images/Image.png";
import "./Form.scss";

function Form() {
  const cardBody1 =
    "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.";
  const cardBody2 =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed massa leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu varius felis. Integer posuere urna ut mi porta, vel fringilla dolor convallis. Sed laoreet sodales sapien vel vestibulum. Nulla dictum eros ut efficitur porttitor. Mauris dignissim, lectus sit amet euismod pretium, ex purus condimentum erat, eu feugiat nisi ipsum ac eros.";

  const [formState, setFormState] = useState({
    name: "",
    mobile: "",
    email: "",
    config: "manual",
  });
  const [showModalState, setShowModal] = useState(false);

  const handleInputChange = (event) => {
    const value = event.target.value;

    setFormState({
      ...formState,
      [event.target.name]: value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setShowModal({
      showModal: true,
    });
    document.body.style.overflow = "hidden";
  };
  const handleModalClose = () => {
    setShowModal({
      showModal: false,
    });
    document.body.style.overflow = "visible";
  };

  const { name, mobile, email, config } = formState;
  const { showModal } = showModalState;

  return (
    <form onSubmit={handleSubmit} data-testid="form" className="form">
      <div className="form__account-info">
        <div className="account-info__image">
          <img src={hero} alt="profile of man" />
        </div>
        <div className="account-info__field">
          <h1>MY ACCOUNT</h1>
          <div className="field">
            <Input
              inputType="text"
              name="name"
              value={name}
              className="field__input"
              handleInputChange={handleInputChange}
            />
            <label htmlFor="name" title="Name">
              Name
            </label>
          </div>

          <div className="account-info__field-group">
            <div className="field">
              <Input
                inputType="tel"
                name="mobile"
                value={mobile}
                className="field__input"
                handleInputChange={handleInputChange}
              />
              <label htmlFor="mobile" title="Mobile">
                Mobile
              </label>
            </div>
            <div className="field">
              <Input
                inputType="text"
                name="email"
                value={email}
                className="field__input"
                handleInputChange={handleInputChange}
              />
              <label htmlFor="email" title="Email">
                Email
              </label>
            </div>
          </div>
        </div>
      </div>

      <hr />

      <div className="form__config">
        <h2>Select Configuration</h2>
        <div className="config">
          <div className="config__option">
            <label className="radio">
              <span className="radio__input">
                <Input
                  inputType="radio"
                  name="config"
                  value="manual"
                  checked={config === "manual"}
                  handleInputChange={handleInputChange}
                />
                <span className="radio__control" />
              </span>
              <span className="radio__label">
                <ConfigCard
                  cardBody={cardBody1}
                  cardTitle="Manual Configuration"
                  cardState={config === "manual" ? "active" : "inactive"}
                />
                <ConfigCard
                  cardBody={cardBody2}
                  cardTitle="Services Access"
                  cardState={
                    config === "manual"
                      ? "active config-card--demphasized"
                      : "inactive config-card--demphasized"
                  }
                />
              </span>
            </label>
          </div>

          <div className="config__option">
            <label className="radio">
              <span className="radio__input">
                <Input
                  inputType="radio"
                  name="config"
                  value="semi-auto"
                  checked={config === "semi-auto"}
                  handleInputChange={handleInputChange}
                />
                <span className="radio__control" />
              </span>
              <span className="radio__label">
                <ConfigCard
                  cardBody={cardBody1}
                  cardTitle="Semi-auto Configuration"
                  cardState={config === "semi-auto" ? "active" : "inactive"}
                />
              </span>
            </label>
          </div>

          <div className="config__option">
            <label className="radio">
              <span className="radio__input">
                <Input
                  inputType="radio"
                  name="config"
                  value="automatic"
                  checked={config === "automatic"}
                  handleInputChange={handleInputChange}
                />
                <span className="radio__control" />
              </span>
              <span className="radio__label">
                <ConfigCard
                  cardBody={cardBody1}
                  cardTitle="Automatic Configuration"
                  cardState={config === "automatic" ? "active" : "inactive"}
                />
              </span>
            </label>
          </div>
        </div>
      </div>
      <div className="form__buttons">
        <Button
          btnType="submit"
          icon={switchIcon}
          value="Update Configuration"
          btnCategory="primary"
        />
        <Button
          btnType="button"
          icon={exit}
          value="Cancel"
          btnCategory="secondary"
        />
      </div>
      {showModal && <Modal handleClick={handleModalClose} />}
    </form>
  );
}
export default Form;
