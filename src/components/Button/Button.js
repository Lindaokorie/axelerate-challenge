import React from "react";
import PropTypes from "prop-types";

import "./Button.scss";

export default function Button({
  btnCategory,
  btnType,
  handleClick,
  icon,
  value,
}) {
  const className = `button button_${btnCategory}`;
  return (
    <button data-testid="button" type={btnType} onClick={handleClick} className={className}>
      <div>
        <img src={icon} alt="button icon" />
      </div>
      {value}
    </button>
  );
}
Button.defaultProps = {
  btnCategory: "primary",
  btnType: "button",
  value: "Update Configuration",
};
Button.propTypes = {
  btnCategory: PropTypes.string,
  btnType: PropTypes.string,
  handleClick: PropTypes.func,
  value: PropTypes.string.isRequired,
  icon: PropTypes.string,
};
