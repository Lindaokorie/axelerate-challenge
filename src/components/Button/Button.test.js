import { render, fireEvent } from '@testing-library/react';
import Button from './Button';


const props = {
        btnCategory: 'primary',
        btnType: 'button',
        handleClick: jest.fn(),
        value: 'Cancel'
    } ;

describe('Button', () => {
    it('renders a button', () => {
        const {getByTestId} = render(<Button/>);
        expect(getByTestId('button')).toBeInTheDocument();
        expect(getByTestId('button').tagName).toBe("BUTTON");
    })
    describe('Button Category', () => {
        it('adds a className to button based on btnCategory', () => {
            const {getByTestId} = render(<Button btnCategory="secondary"/>);
            const btnClassName = getByTestId('button').className;
            expect(btnClassName.includes("button_secondary")).toBe(true);
        })

        it('adds a className to button_primary by default if no other category is passed', () => {
            const {getByTestId} = render(<Button />);
            const btnClassName = getByTestId('button').className;
            expect(btnClassName.includes("button_primary")).toBe(true);
        })
    })
    
    describe('Button Type', () => {
       it('adds a type attribute to button based on btnType', () => {
           const {getByTestId} = render(<Button btnType="submit"/>);
           expect(getByTestId('button').getAttribute('type')).toBe('submit');
        }) 

        it('adds a default type of button to button', () => {
           const {getByTestId} = render(<Button />);
           expect(getByTestId('button').getAttribute('type')).toBe('button');
        }) 
    })
    
    
    it('adds text to button based on value', () => {
        const {getByText} = render(<Button value="Click Me!"/>);
        expect(getByText('Click Me!')).toBeInTheDocument();
    })

    it('renders an image with the icon url', () => {
        const {getByAltText} = render(<Button icon="/image.png"/>);
        const buttonImage = getByAltText('button icon');
        expect(buttonImage).toBeInTheDocument();
        expect(buttonImage.getAttribute('src')).toBe('/image.png');
    })

    it('calls handleClick prop', () => {
        const handleClick = jest.fn();
        const {getByTestId} = render(<Button handleClick={handleClick}/>);
        expect(handleClick).not.toHaveBeenCalled();
        fireEvent.click(getByTestId('button'));
        expect(handleClick).toHaveBeenCalled();
    })
})