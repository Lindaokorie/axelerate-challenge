import React from 'react'

import Nav from '../Nav/Nav'
import './Header.scss'
import warning from '../../images/warning.svg'

export default function Header() {
    
    return (
        <header>
            <div className="header__warning">
                <div className="header__warning-icon">
                    <img src={warning} alt="warning icon"/>
                </div>
                <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.</p>
            </div>
            <Nav/> 
        </header>
    )
}
