import React, { useState } from "react";

import SideNav from "../SideNav/SideNav";
import logo from "../../images/Logo.svg";
import logOut from "../../images/logout.svg";
import "./Nav.scss";

export default function Nav() {
  const [showNav, setShowNav] = useState(false);
  const togggleNav = () => {
    if (showNav) {
      setShowNav(false);
      document.body.style.overflow = "visible";
    } else {
      setShowNav(true);
      document.body.style.overflow = "hidden";
    }
  };
  return (
    <>
      <div className="nav">
        <div className="nav__menu-icon" onClick={togggleNav}>
          <span className={`bar1 ${showNav && "change"}`} />
          <span className={`bar2 ${showNav && "change"}`} />
          <span className={`bar3 ${showNav && "change"}`} />
        </div>
        <div className="nav__logo">
          <img src={logo} alt="site logo" />
        </div>
        <div className="nav__menu-items">
          <nav>
            <ul>
              <li>
                <a href="#" className="nav__menu-item">
                  Dashboard
                </a>
              </li>
              <li>
                <a href="#" className="nav__menu-item nav__menu-item--active">
                  Account
                </a>
              </li>
              <li>
                <a href="#" className="nav__menu-item">
                  Help
                </a>
              </li>
              <li>
                <a href="#" className="nav__menu-item">
                  Logout
                </a>
              </li>
            </ul>
          </nav>
        </div>
        <div className="nav__log-out">
          <img src={logOut} alt="log out icon" />
        </div>
      </div>
      <SideNav showNav={showNav} />
    </>
  );
}
