import React from "react";
import PropTypes from "prop-types";

import "./SideNav.scss";

export default function SideNav({ showNav }) {
  const className = `side-nav ${showNav ? "showNav" : "hideNav"}`;
  return (
    <nav className={className}>
      <ul>
        <li>
          <a href="#" className="side-nav__item">
            Dashboard
          </a>
        </li>
        <li>
          <a href="#" className="side-nav__item side-nav__item--active">
            Account
          </a>
        </li>
        <li>
          <a href="#" className="side-nav__item">
            Help
          </a>
        </li>
      </ul>
    </nav>
  );
}
SideNav.propTypes = {
  showNav: PropTypes.bool.isRequired,
};
