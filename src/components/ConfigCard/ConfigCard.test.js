import { render } from '@testing-library/react';
import ConfigCard from '../components/ConfigCard';

test('renders input', () => {
    const props = {
        cardBody: 'Lorem ipsum',
        cardTitle: 'Manual Configuration',
        cardState: 'manual'
    } ;
    render(<ConfigCard {...props}/>)
})