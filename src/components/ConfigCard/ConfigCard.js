import React from "react";
import PropTypes from "prop-types";

import "./ConfigCard.scss";

export default function ConfigCard({ cardState, cardTitle, cardBody }) {
  const className = `config-card config-card--${cardState}`;
  return (
    <div className={className}>
      <h3>{cardTitle}</h3>
      <p>{cardBody}</p>
    </div>
  );
}
ConfigCard.propTypes = {
  cardBody: PropTypes.string.isRequired,
  cardTitle: PropTypes.string.isRequired,
  cardState: PropTypes.string.isRequired,
};
