import React from "react";

import Header from "./components/Header/Header";
import Form from "./components/Form/Form";
import "./App.scss";

export default function App() {
  return (
    <div className="App">
      <Header />
      <Form />
    </div>
  );
}
