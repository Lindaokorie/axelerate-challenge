const verifyAge = (age, min = 18, max = 65) => {
  if (
    typeof age === "number" &&
    typeof min === "number" &&
    typeof max === "number"
  ) {
    if (age <= 0 || min <= 0 || max <= 0) {
      return "Invalid argument(s)";
    } else if (age < min || age > max) {
      return false;
    } else {
      return true;
    }
  } else {
    return NaN;
  }
};

export default verifyAge;
