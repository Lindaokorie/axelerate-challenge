import verifyAge from ".";

describe("verifyAge", () => {
  describe("When given valid arguments", () => {
    it("returns false when the age is below the minimum age", () => {
      expect(verifyAge(14, 26, 65)).toBe(false);
    });
    it("returns true when age falls between minimum and maximum age", () => {
      expect(verifyAge(27, 22, 45)).toBe(true);
    });
    it("returns false when age is above the maximum age", () => {
      expect(verifyAge(75, 23, 55)).toBe(false);
    });
    it("returns when max argument is omitted", () => {
      expect(verifyAge(14, 23)).toBe(false);
    });
    it("returns when min are max arguments are omitted", () => {
      expect(verifyAge(21)).toBe(true);
    });
  });
  describe("When given invalid arguments", () => {
    it("returns NaN when age passed is not a number", () => {
      expect(verifyAge("twelve", 34, 65)).toBeNaN();
    });
    it("returns NaN when min age passed is not a number", () => {
      expect(verifyAge(12, {}, 76)).toBeNaN();
    });
    it("returns NaN when max age passed is not a number", () => {
      expect(verifyAge(12, 32, {})).toBeNaN();
    });
    it("does not return when age is 0 or less than 0", () => {
      expect(verifyAge(0, 23, 55)).toBe("Invalid argument(s)");
    });
    it("does not return when min is 0 or less than 0", () => {
      expect(verifyAge(23, 0, 55)).toBe("Invalid argument(s)");
    });
    it("does not return when max is 0 or less than 0", () => {
      expect(verifyAge(23, 32, 0)).toBe("Invalid argument(s)");
    });
  });
});
