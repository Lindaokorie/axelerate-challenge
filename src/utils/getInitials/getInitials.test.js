import getInitials from './';


describe('getInitials', () => {
    it('gets initials from a given name', () => {
        expect(getInitials("Linda Okorie")).toEqual("LO");
    });

    it('returns an empty string if argument type is not String', () => {
        expect(getInitials({})).toEqual("");
    });

    it('returns two Initials if argument has more than two names', () => {
        const initials= getInitials("Linda Chidinma Okorie");
        expect(initials).toHaveLength(2);
        expect(initials).toEqual("LC");
    });

    it('returns an empty string if an empty is passed', () => {
        expect(getInitials("")).toEqual("");
    });

    it('returns one initial if the argument has just one name', () => {
        const initials= getInitials("Linda");
        expect(initials).toHaveLength(1);
        expect(initials).toEqual("L");
    })
})