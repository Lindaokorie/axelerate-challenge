const getInitials = (name) => {
  if (typeof name === "String" || !name.length) {
    return "";
  }
  const [firstname, lastname] = name.split(" ");
  return `${firstname.charAt(0).toUpperCase()}${
    lastname ? lastname.charAt(0).toUpperCase() : ""
  }`;
};

export default getInitials;
